package com.lauchenauer.timetracker.web.rest;

import com.lauchenauer.timetracker.TimetrackerApp;
import com.lauchenauer.timetracker.domain.Project;
import com.lauchenauer.timetracker.domain.TimeEntry;
import com.lauchenauer.timetracker.domain.User;
import com.lauchenauer.timetracker.service.ReportsService;
import com.lauchenauer.timetracker.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;

import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the TimeEntryResource REST controller.
 *
 * @see TimeEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimetrackerApp.class)
public class ReportsResourceIntTest {
    @Inject
    private ReportsService reportsService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private EntityManager em;

    private MockMvc restReportsMockMvc;
    private UserService userService;
    private User admin;



    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReportsResource reportsResource = new ReportsResource();

        admin = new User();
        admin.setLogin("admin");
        admin.setId(3l);
        userService = mock(UserService.class);
        when(userService.getUserWithAuthorities()).thenReturn(admin);

        ReflectionTestUtils.setField(reportsResource, "reportsService", reportsService);
        ReflectionTestUtils.setField(reportsResource, "userService", userService);
        this.restReportsMockMvc = MockMvcBuilders.standaloneSetup(reportsResource)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        // projects
        Project project1 = new Project().name("Test Project 1");
        Project project2 = new Project().name("Test Project 2");
        em.persist(project1);
        em.persist(project2);

        // time entries
        TimeEntry timeEntry1 = new TimeEntry().time(8.0).date(LocalDate.now()).project(project1).user(admin);
        TimeEntry timeEntry2 = new TimeEntry().time(8.0).date(LocalDate.now()).project(project1).user(admin);
        em.persist(timeEntry1);
        em.persist(timeEntry2);
    }

    @Test
    @Transactional
    public void getByUsersSummary() throws Exception {
        restReportsMockMvc.perform(get("/api/reports/by_user_summary"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].user_name").value(hasItem("admin")))
            .andExpect(jsonPath("$.[*].time").value(hasItem(16.0)));
    }

    @Test
    @Transactional
    public void getByUserAndProject() throws Exception {
        restReportsMockMvc.perform(get("/api/reports/by_user_and_project"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[0].user_name").value("admin"))
            .andExpect(jsonPath("$.[0].time").value(16.0))
            .andExpect(jsonPath("$.[0].project_name").value("Test Project 1"));
    }

    @Test
    @Transactional
    public void getByProjectForUserSummary() throws Exception {
        restReportsMockMvc.perform(get("/api/reports/by_project_for_user_summary"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].project_name").value(hasItem("Test Project 1")))
            .andExpect(jsonPath("$.[*].time").value(hasItem(16.0)));
    }
}
