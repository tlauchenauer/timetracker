package com.lauchenauer.timetracker.web.rest;

import com.lauchenauer.timetracker.TimetrackerApp;
import com.lauchenauer.timetracker.domain.Project;
import com.lauchenauer.timetracker.domain.TimeEntry;
import com.lauchenauer.timetracker.repository.TimeEntryRepository;
import com.lauchenauer.timetracker.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the TimeEntryResource REST controller.
 *
 * @see TimeEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimetrackerApp.class)
public class TimeEntryResourceIntTest {

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_TIME = 0D;
    private static final Double UPDATED_TIME = 1D;
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private TimeEntryRepository timeEntryRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private UserService userService;

    @Inject
    private EntityManager em;

    private MockMvc restTimeEntryMockMvc;

    private TimeEntry timeEntry;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TimeEntryResource timeEntryResource = new TimeEntryResource();
        ReflectionTestUtils.setField(timeEntryResource, "timeEntryRepository", timeEntryRepository);
        ReflectionTestUtils.setField(timeEntryResource, "userService", userService);
        this.restTimeEntryMockMvc = MockMvcBuilders.standaloneSetup(timeEntryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TimeEntry createEntity(EntityManager em) {
        TimeEntry timeEntry = new TimeEntry();
        timeEntry = new TimeEntry()
                .date(DEFAULT_DATE)
                .time(DEFAULT_TIME)
                .description(DEFAULT_DESCRIPTION);
        // Add required entity
        Project project = ProjectResourceIntTest.createEntity(em);
        em.persist(project);
        em.flush();
        timeEntry.setProject(project);
        return timeEntry;
    }

    @Before
    public void initTest() {
        timeEntry = createEntity(em);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = timeEntryRepository.findAll().size();
        // set the field null
        timeEntry.setDate(null);

        // Create the TimeEntry, which fails.

        restTimeEntryMockMvc.perform(post("/api/time-entries")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(timeEntry)))
                .andExpect(status().isBadRequest());

        List<TimeEntry> timeEntries = timeEntryRepository.findAll();
        assertThat(timeEntries).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = timeEntryRepository.findAll().size();
        // set the field null
        timeEntry.setTime(null);

        // Create the TimeEntry, which fails.

        restTimeEntryMockMvc.perform(post("/api/time-entries")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(timeEntry)))
                .andExpect(status().isBadRequest());

        List<TimeEntry> timeEntries = timeEntryRepository.findAll();
        assertThat(timeEntries).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getNonExistingTimeEntry() throws Exception {
        // Get the timeEntry
        restTimeEntryMockMvc.perform(get("/api/time-entries/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }
}
