(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
