(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('by-project-for-user', {
            parent: 'reports',
            url: '/reports/by_project_for_user_summary',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Summary by Project for User'
            },
            views: {
                'content@': {
                    templateUrl: 'app/reports/by-project-for-user/by-project-for-user.html',
                    controller: 'ByProjectForUserController',
                    controllerAs: 'vm'
                }
            }
        })
    }

})();
