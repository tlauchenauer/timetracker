(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .controller('ByProjectForUserController', ByProjectForUserController);

    ByProjectForUserController.$inject = ['$scope', '$state', 'ByProjectForUser'];

    function ByProjectForUserController ($scope, $state, ByProjectForUser) {
        var vm = this;

        loadAll();

        function loadAll () {
            ByProjectForUser.query({}, onSuccess);
            function onSuccess(data) {
                vm.data = data;
            }
        }
    }
})();
