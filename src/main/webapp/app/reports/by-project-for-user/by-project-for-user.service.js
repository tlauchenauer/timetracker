(function() {
    'use strict';
    angular
        .module('timetrackerApp')
        .factory('ByProjectForUser', ByProjectForUser);

    ByProjectForUser.$inject = ['$resource'];

    function ByProjectForUser ($resource) {
        var resourceUrl =  'api/reports/by_project_for_user_summary';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
