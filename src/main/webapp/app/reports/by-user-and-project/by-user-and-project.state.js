(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('by-user-and-project', {
            parent: 'reports',
            url: '/reports/by_user_and_project',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'By User and Project'
            },
            views: {
                'content@': {
                    templateUrl: 'app/reports/by-user-and-project/by-user-and-project.html',
                    controller: 'ByUserAndProjectController',
                    controllerAs: 'vm'
                }
            }
        })
    }

})();
