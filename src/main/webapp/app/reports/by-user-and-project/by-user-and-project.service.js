(function() {
    'use strict';
    angular
        .module('timetrackerApp')
        .factory('ByUserAndProject', ByUserAndProject);

    ByUserAndProject.$inject = ['$resource'];

    function ByUserAndProject ($resource) {
        var resourceUrl =  'api/reports/by_user_and_project';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
