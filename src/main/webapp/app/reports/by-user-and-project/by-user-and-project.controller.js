(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .controller('ByUserAndProjectController', ByUserAndProjectController);

    ByUserAndProjectController.$inject = ['$scope', '$state', 'ByUserAndProject'];

    function ByUserAndProjectController ($scope, $state, ByUserAndProject) {
        var vm = this;

        loadAll();

        function loadAll () {
            ByUserAndProject.query({}, onSuccess);
            function onSuccess(data) {
                vm.data = data;
            }
        }
    }
})();
