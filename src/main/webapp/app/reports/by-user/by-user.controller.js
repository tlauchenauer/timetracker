(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .controller('ByUserController', ByUserController);

    ByUserController.$inject = ['$scope', '$state', 'ByUser'];

    function ByUserController ($scope, $state, ByUser) {
        var vm = this;

        loadAll();

        function loadAll () {
            ByUser.query({}, onSuccess);
            function onSuccess(data) {
                vm.data = data;
            }
        }
    }
})();
