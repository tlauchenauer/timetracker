(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('by-user', {
            parent: 'reports',
            url: '/reports/by_user_summary',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Summary by User'
            },
            views: {
                'content@': {
                    templateUrl: 'app/reports/by-user/by-user.html',
                    controller: 'ByUserController',
                    controllerAs: 'vm'
                }
            }
        })
    }

})();
