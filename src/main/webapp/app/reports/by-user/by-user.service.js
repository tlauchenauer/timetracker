(function() {
    'use strict';
    angular
        .module('timetrackerApp')
        .factory('ByUser', ByUser);

    ByUser.$inject = ['$resource'];

    function ByUser ($resource) {
        var resourceUrl =  'api/reports/by_user_summary';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
