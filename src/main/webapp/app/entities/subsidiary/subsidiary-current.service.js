(function() {
    'use strict';
    angular
        .module('timetrackerApp')
        .factory('SubsidiaryCurrent', SubsidiaryCurrent);

    SubsidiaryCurrent.$inject = ['$resource'];

    function SubsidiaryCurrent ($resource) {
        var resourceUrl =  'api/subsidiaries-current-user';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
        });
    }
})();
