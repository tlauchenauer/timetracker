(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .controller('TimeEntryDeleteController',TimeEntryDeleteController);

    TimeEntryDeleteController.$inject = ['$uibModalInstance', 'entity', 'TimeEntry'];

    function TimeEntryDeleteController($uibModalInstance, entity, TimeEntry) {
        var vm = this;

        vm.timeEntry = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TimeEntry.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
