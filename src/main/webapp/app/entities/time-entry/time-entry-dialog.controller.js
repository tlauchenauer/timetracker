(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .controller('TimeEntryDialogController', TimeEntryDialogController);

    TimeEntryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TimeEntry', 'Project', 'User', "SubsidiaryCurrent"];

    function TimeEntryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TimeEntry, Project, User, SubsidiaryCurrent) {
        var vm = this;

        vm.timeEntry = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.projects = Project.query();
        vm.users = User.query();
        vm.subsidiaries = SubsidiaryCurrent.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.timeEntry.id !== null) {
                TimeEntry.update(vm.timeEntry, onSaveSuccess, onSaveError);
            } else {
                TimeEntry.save(vm.timeEntry, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('timetrackerApp:timeEntryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
