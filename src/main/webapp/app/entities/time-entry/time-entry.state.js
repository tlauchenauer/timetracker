(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('time-entry', {
            parent: 'entity',
            url: '/time-entry?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TimeEntries'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/time-entry/time-entries.html',
                    controller: 'TimeEntryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('time-entry-detail', {
            parent: 'entity',
            url: '/time-entry/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'TimeEntry'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/time-entry/time-entry-detail.html',
                    controller: 'TimeEntryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'TimeEntry', function($stateParams, TimeEntry) {
                    return TimeEntry.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'time-entry',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('time-entry-detail.edit', {
            parent: 'time-entry-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/time-entry/time-entry-dialog.html',
                    controller: 'TimeEntryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TimeEntry', function(TimeEntry) {
                            return TimeEntry.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('time-entry.new', {
            parent: 'time-entry',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/time-entry/time-entry-dialog.html',
                    controller: 'TimeEntryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                date: null,
                                time: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('time-entry', null, { reload: 'time-entry' });
                }, function() {
                    $state.go('time-entry');
                });
            }]
        })
        .state('time-entry.edit', {
            parent: 'time-entry',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/time-entry/time-entry-dialog.html',
                    controller: 'TimeEntryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TimeEntry', function(TimeEntry) {
                            return TimeEntry.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('time-entry', null, { reload: 'time-entry' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('time-entry.delete', {
            parent: 'time-entry',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/time-entry/time-entry-delete-dialog.html',
                    controller: 'TimeEntryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TimeEntry', function(TimeEntry) {
                            return TimeEntry.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('time-entry', null, { reload: 'time-entry' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
