(function() {
    'use strict';

    angular
        .module('timetrackerApp')
        .controller('TimeEntryDetailController', TimeEntryDetailController);

    TimeEntryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TimeEntry', 'Project', 'User'];

    function TimeEntryDetailController($scope, $rootScope, $stateParams, previousState, entity, TimeEntry, Project, User) {
        var vm = this;

        vm.timeEntry = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('timetrackerApp:timeEntryUpdate', function(event, result) {
            vm.timeEntry = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
