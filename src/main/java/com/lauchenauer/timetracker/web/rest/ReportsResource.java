package com.lauchenauer.timetracker.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.lauchenauer.timetracker.service.ReportsService;
import com.lauchenauer.timetracker.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/reports")
public class ReportsResource {

    private final Logger log = LoggerFactory.getLogger(ReportsResource.class);

    @Inject
    private ReportsService reportsService;

    @Inject
    private UserService userService;



    @RequestMapping(value = "/by_project_for_user_summary",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Map<String, Object>>> getSummaryByProjectForUserReport()
        throws URISyntaxException {
        List<Map<String, Object>> data = reportsService.getSummaryByProjectForUserReport(userService.getUserWithAuthorities().getId());

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/by_user_summary",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Map<String, Object>>> getSummaryByUserReport()
        throws URISyntaxException {
        List<Map<String, Object>> data = reportsService.getSummaryByUserReport();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/by_user_and_project",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Map<String, Object>>> getSummaryByUserAndProjectReport()
        throws URISyntaxException {
        List<Map<String, Object>> data = reportsService.getSummaryByUserAndProjectReport();

        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
