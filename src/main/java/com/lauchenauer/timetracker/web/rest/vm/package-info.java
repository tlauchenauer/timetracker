/**
 * View Models used by Spring MVC REST controllers.
 */
package com.lauchenauer.timetracker.web.rest.vm;
