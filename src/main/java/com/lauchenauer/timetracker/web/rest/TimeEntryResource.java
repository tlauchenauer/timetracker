package com.lauchenauer.timetracker.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.lauchenauer.timetracker.domain.Subsidiary;
import com.lauchenauer.timetracker.domain.TimeEntry;
import com.lauchenauer.timetracker.domain.User;
import com.lauchenauer.timetracker.repository.SubsidiaryRepository;
import com.lauchenauer.timetracker.repository.TimeEntryRepository;
import com.lauchenauer.timetracker.security.SecurityUtils;
import com.lauchenauer.timetracker.service.UserService;
import com.lauchenauer.timetracker.web.rest.util.HeaderUtil;
import com.lauchenauer.timetracker.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TimeEntry.
 */
@RestController
@RequestMapping("/api")
public class TimeEntryResource {

    private final Logger log = LoggerFactory.getLogger(TimeEntryResource.class);

    @Inject
    private TimeEntryRepository timeEntryRepository;

    @Inject
    private SubsidiaryRepository subsidiaryRepository;

    @Inject
    private UserService userService;

    /**
     * POST  /time-entries : Create a new timeEntry.
     *
     * @param timeEntry the timeEntry to create
     * @return the ResponseEntity with status 201 (Created) and with body the new timeEntry, or with status 400 (Bad Request) if the timeEntry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/time-entries",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TimeEntry> createTimeEntry(@Valid @RequestBody TimeEntry timeEntry) throws URISyntaxException {
        log.debug("REST request to save TimeEntry : {}", timeEntry);
        if (timeEntry.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("timeEntry", "idexists", "A new timeEntry cannot already have an ID")).body(null);
        }

        if (timeEntry.getUser() == null) {
            timeEntry.setUser(userService.getUserWithAuthorities());
        }

        if (!isCurrentUserAllowedToSubmitForUser(timeEntry.getUser())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        TimeEntry result = timeEntryRepository.save(timeEntry);
        return ResponseEntity.created(new URI("/api/time-entries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("timeEntry", result.getId().toString()))
            .body(result);
    }

    private boolean isCurrentUserAllowedToSubmitForUser(User user) {
        User currentUser = userService.getUserWithAuthorities();
        if (currentUser.equals(user)) return true;

        List<Subsidiary> subsidiaries = subsidiaryRepository.findBySubsidiaryIsCurrentUser();
        for (Subsidiary sub : subsidiaries) {
            if (sub.getOwner().equals(user)) return true;
        }

        return false;
    }

    /**
     * PUT  /time-entries : Updates an existing timeEntry.
     *
     * @param timeEntry the timeEntry to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated timeEntry,
     * or with status 400 (Bad Request) if the timeEntry is not valid,
     * or with status 500 (Internal Server Error) if the timeEntry couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/time-entries",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TimeEntry> updateTimeEntry(@Valid @RequestBody TimeEntry timeEntry) throws URISyntaxException {
        log.debug("REST request to update TimeEntry : {}", timeEntry);
        if (timeEntry.getId() == null) {
            return createTimeEntry(timeEntry);
        }
        timeEntry.setUser(userService.getUserWithAuthorities());
        TimeEntry result = timeEntryRepository.save(timeEntry);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("timeEntry", timeEntry.getId().toString()))
            .body(result);
    }

    /**
     * GET  /time-entries : get all the timeEntries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of timeEntries in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/time-entries",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TimeEntry>> getAllTimeEntries(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TimeEntries");
        Page<TimeEntry> page = timeEntryRepository.findByUserIsCurrentUser(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/time-entries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /time-entries/:id : get the "id" timeEntry.
     *
     * @param id the id of the timeEntry to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the timeEntry, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/time-entries/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TimeEntry> getTimeEntry(@PathVariable Long id) {
        log.debug("REST request to get TimeEntry : {}", id);
        TimeEntry timeEntry = timeEntryRepository.findOne(id);

        if (timeEntry != null && !SecurityUtils.isCurrentUserAdminOrOwner(timeEntry.getUser())) {
             return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return Optional.ofNullable(timeEntry)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /time-entries/:id : delete the "id" timeEntry.
     *
     * @param id the id of the timeEntry to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/time-entries/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTimeEntry(@PathVariable Long id) {
        log.debug("REST request to delete TimeEntry : {}", id);

        TimeEntry timeEntry = timeEntryRepository.findOne(id);
        if (timeEntry != null && !SecurityUtils.isCurrentUserAdminOrOwner(timeEntry.getUser())) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        timeEntryRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("timeEntry", id.toString())).build();
    }

}
