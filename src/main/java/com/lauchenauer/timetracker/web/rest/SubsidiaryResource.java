package com.lauchenauer.timetracker.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.lauchenauer.timetracker.domain.Subsidiary;
import com.lauchenauer.timetracker.domain.User;
import com.lauchenauer.timetracker.repository.SubsidiaryRepository;
import com.lauchenauer.timetracker.web.rest.util.HeaderUtil;
import com.lauchenauer.timetracker.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Subsidiary.
 */
@RestController
@RequestMapping("/api")
public class SubsidiaryResource {

    private final Logger log = LoggerFactory.getLogger(SubsidiaryResource.class);

    @Inject
    private SubsidiaryRepository subsidiaryRepository;

    /**
     * POST  /subsidiaries : Create a new subsidiary.
     *
     * @param subsidiary the subsidiary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subsidiary, or with status 400 (Bad Request) if the subsidiary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/subsidiaries",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subsidiary> createSubsidiary(@Valid @RequestBody Subsidiary subsidiary) throws URISyntaxException {
        log.debug("REST request to save Subsidiary : {}", subsidiary);
        if (subsidiary.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("subsidiary", "idexists", "A new subsidiary cannot already have an ID")).body(null);
        }
        Subsidiary result = subsidiaryRepository.save(subsidiary);
        return ResponseEntity.created(new URI("/api/subsidiaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("subsidiary", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subsidiaries : Updates an existing subsidiary.
     *
     * @param subsidiary the subsidiary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subsidiary,
     * or with status 400 (Bad Request) if the subsidiary is not valid,
     * or with status 500 (Internal Server Error) if the subsidiary couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/subsidiaries",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subsidiary> updateSubsidiary(@Valid @RequestBody Subsidiary subsidiary) throws URISyntaxException {
        log.debug("REST request to update Subsidiary : {}", subsidiary);
        if (subsidiary.getId() == null) {
            return createSubsidiary(subsidiary);
        }
        Subsidiary result = subsidiaryRepository.save(subsidiary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("subsidiary", subsidiary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subsidiaries : get all the subsidiaries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subsidiaries in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/subsidiaries",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Subsidiary>> getAllSubsidiaries(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Subsidiaries");
        Page<Subsidiary> page = subsidiaryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subsidiaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /subsidiaries/:id : get the "id" subsidiary.
     *
     * @param id the id of the subsidiary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subsidiary, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/subsidiaries/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subsidiary> getSubsidiary(@PathVariable Long id) {
        log.debug("REST request to get Subsidiary : {}", id);
        Subsidiary subsidiary = subsidiaryRepository.findOne(id);
        return Optional.ofNullable(subsidiary)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /subsidiaries-current-user
     *
     * @return the ResponseEntity with status 200 (OK) and with body the subsidiary, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/subsidiaries-current-user",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<User>> getSubsidiary() {
        log.debug("REST request to get list of users that the current user is a subsidiary for");
        List<Subsidiary> subsidiaries = subsidiaryRepository.findBySubsidiaryIsCurrentUser();
        List<User> users = new ArrayList<>();
        for (Subsidiary sub : subsidiaries) {
            users.add(sub.getOwner());
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    /**
     * DELETE  /subsidiaries/:id : delete the "id" subsidiary.
     *
     * @param id the id of the subsidiary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/subsidiaries/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSubsidiary(@PathVariable Long id) {
        log.debug("REST request to delete Subsidiary : {}", id);
        subsidiaryRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("subsidiary", id.toString())).build();
    }

}
