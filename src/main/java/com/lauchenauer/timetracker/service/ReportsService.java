package com.lauchenauer.timetracker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ReportsService {

    private final Logger log = LoggerFactory.getLogger(ReportsService.class);

    @Inject
    private EntityManager entityManager;

    public List<Map<String, Object>> getSummaryByProjectForUserReport(long userId) {
        Query q =
            entityManager.createNativeQuery("SELECT p.name, SUM(te.time) FROM project p " +
                                            "LEFT JOIN time_entry te ON te.project_id = p.id " +
                                            "WHERE te.user_id = " + userId +
                                            " OR te.user_id IS NULL " +
                                            "GROUP BY p.id;"
        );

        List<Object[]> results = q.getResultList();

        List<Map<String, Object>> reportData = new ArrayList<>();
        for (Object[] row : results) {
            Map<String, Object> rowData = new HashMap<>();
            rowData.put("project_name", row[0]);
            rowData.put("time", row[1]);
            reportData.add(rowData);
        }

        return reportData;
    }

    public List<Map<String, Object>> getSummaryByUserReport() {
        Query q =
            entityManager.createNativeQuery("SELECT u.login, SUM(te.time) FROM jhi_user u " +
                                            "LEFT JOIN time_entry te on te.user_id = u.id " +
                                            "GROUP BY u.id;"
            );

        List<Object[]> results = q.getResultList();

        List<Map<String, Object>> reportData = new ArrayList<>();
        for (Object[] row : results) {
            Map<String, Object> rowData = new HashMap<>();
            rowData.put("user_name", row[0]);
            rowData.put("time", row[1]);
            reportData.add(rowData);
        }

        return reportData;
    }

    public List<Map<String, Object>> getSummaryByUserAndProjectReport() {
        Query q =
            entityManager.createNativeQuery("SELECT u.login, p.name, sum(te.time) FROM time_entry te " +
                                            "JOIN jhi_user u ON te.user_id = u.id " +
                                            "JOIN project p ON p.id = te.project_id " +
                                            "GROUP BY u.login, p.name;"
            );

        List<Object[]> results = q.getResultList();

        List<Map<String, Object>> reportData = new ArrayList<>();
        for (Object[] row : results) {
            Map<String, Object> rowData = new HashMap<>();
            rowData.put("user_name", row[0]);
            rowData.put("project_name", row[1]);
            rowData.put("time", row[2]);
            reportData.add(rowData);
        }

        return reportData;
    }

}
