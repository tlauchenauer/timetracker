package com.lauchenauer.timetracker.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Subsidiary.
 */
@Entity
@Table(name = "subsidiary")
public class Subsidiary implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @NotNull
    private User owner;

    @ManyToOne
    @NotNull
    private User subsidiary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public Subsidiary owner(User user) {
        this.owner = user;
        return this;
    }

    public void setOwner(User user) {
        this.owner = user;
    }

    public User getSubsidiary() {
        return subsidiary;
    }

    public Subsidiary subsidiary(User user) {
        this.subsidiary = user;
        return this;
    }

    public void setSubsidiary(User user) {
        this.subsidiary = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subsidiary subsidiary = (Subsidiary) o;
        if(subsidiary.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, subsidiary.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Subsidiary{" +
            "id=" + id +
            '}';
    }
}
