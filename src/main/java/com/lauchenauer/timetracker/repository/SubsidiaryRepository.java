package com.lauchenauer.timetracker.repository;

import com.lauchenauer.timetracker.domain.Subsidiary;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Subsidiary entity.
 */
@SuppressWarnings("unused")
public interface SubsidiaryRepository extends JpaRepository<Subsidiary,Long> {

    @Query("select subsidiary from Subsidiary subsidiary where subsidiary.owner.login = ?#{principal.username}")
    List<Subsidiary> findByOwnerIsCurrentUser();

    @Query("select subsidiary from Subsidiary subsidiary where subsidiary.subsidiary.login = ?#{principal.username}")
    List<Subsidiary> findBySubsidiaryIsCurrentUser();

}
