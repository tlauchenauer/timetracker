package com.lauchenauer.timetracker.repository;

import com.lauchenauer.timetracker.domain.TimeEntry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Spring Data JPA repository for the TimeEntry entity.
 */
@SuppressWarnings("unused")
public interface TimeEntryRepository extends JpaRepository<TimeEntry,Long> {

    @Query("select timeEntry from TimeEntry timeEntry where timeEntry.user.login = ?#{principal.username}")
    Page<TimeEntry> findByUserIsCurrentUser(Pageable pageable);

}
