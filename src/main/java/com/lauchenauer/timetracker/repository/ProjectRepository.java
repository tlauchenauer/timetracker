package com.lauchenauer.timetracker.repository;

import com.lauchenauer.timetracker.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Project entity.
 */
@SuppressWarnings("unused")
public interface ProjectRepository extends JpaRepository<Project,Long> {
    Optional<Project> findOneByName(String name);
}
